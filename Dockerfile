FROM php:7.2-fpm

RUN apt-get update && apt-get install -y \
    git \
    zlib1g-dev \
    mysql-client \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN docker-php-ext-install -j$(nproc) pdo_mysql zip

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && composer --version

COPY --chown=www-data:www-data my-php-app/ /symfony/
COPY res/ /res/

USER www-data

RUN cd /symfony ; touch .env ; composer install

WORKDIR /symfony
